text = ('Привет'
    ' В этом тексте мы будем считать слова'
    ' Надо научиться это делать причем как можно скорее'
    ' Я сегодня пошел в магазин Я купил колбасу Магазин закрылся слова СЛОВА')

print(text)

wordlist = [w.lower() for w in text.split()]

print(wordlist)

word_count = dict()

for w in wordlist:
    if w in word_count:
        word_count[w] = word_count[w] + 1
    else:
        word_count[w] = 1

#выводим слова и их количество без сортировки
print(word_count)

sorted_words = sorted(word_count, key=word_count.get, reverse=True)

#выводим слова в порядке убывания количества, без указания количества
print(sorted_words)

#выводим слова в порядке убывания количества с указанием количества
print([w + ':' + str(word_count[w]) for w in sorted_words])
