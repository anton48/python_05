# У тебя есть список из 5 чисел. надо поменять местами 1 и 5 число, а также 2 и 4 число.

# 1-й вариант (Сизифов труд).
spisok_1 = [10, 11, 12, 13, 14]     # Такая последовательность - для наглядности.
print('Список_1' + '=' + str(spisok_1))
a = 0     # Индекс 1-го элемента.
b = 1     # Индекс 2-го элемента.
d = 3     # Индекс 4-го элемента.
e = 4     # Индекс 5-го элемента.


x = spisok_1[a]     # Заводим отдельную переменную для временного хранения элемента.
spisok_1[a] = spisok_1[e]
spisok_1[e] = x
#print(spisok_1)

x = spisok_1[b]     # Аналогично для второй пары.
spisok_1 [b] = spisok_1[d]
spisok_1[d] = x
print('Список_2' + '=' + str(spisok_1))

# 2-й вариант
spisok_1 = [10, 11, 12, 13, 14]     # Такая последовательность - для наглядности.
#print(spisok_1)
spisok_2 = []
j = len(spisok_1) - 1
indexes = range (0,len(spisok_1))
x = 0
for i in indexes:
    x = spisok_1[j - i]     # Формирую элементы нового списка.
    spisok_2.append(x)
print('Список_2' + '=' + str(spisok_2))

# 3-й вариант через функцию.
def swap(array, a, b):
    array[a], array[b] = array[b], array[a]

swap(spisok_1, 0, 4)
swap(spisok_1, 1, 3)
print(spisok_1)

# (Дополнительное условие) Из каждого числа из этого списка отнять 1 и умножить на 2.

def minus1(a):
    return a - 1

def mult(a):
    return a * 2

print([mult(minus1(x)) for x in spisok_1])
f = lambda a: mult(minus1(a))
print([f(x) for x in spisok_1])