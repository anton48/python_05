roubles = int(input())
kopeiki = int(input())
cakesNum = int(input())
cake1 = roubles * 100 + kopeiki    # Стоимость одного пирожка в копейках.
budgetR = cake1 * cakesNum // 100    # Стоимость N пирожков в рублях.
budgetK = cake1 * cakesNum % 100    # Остаток стоимости N пирожков в копейках.
print(budgetR, budgetK)
