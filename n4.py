#задание 2. считывать числа, вводимые в консоли (по одному на строке, т.е. вводишь число и жмешь enter), в список.
# закончить считывание, когда введено "stop". если введено не число и не "stop", писать сообщение,
# что не удалось распознать число. вывести получившийся список на экран.

spisok = []
should_stop = False
while not should_stop:
    x = input('Введите число: ')
    if x.isdecimal():
        spisok.append(x)
#        if len(spisok) == 5:
#           should_stop = True
    elif x == 'stop':
        should_stop = True
    else:
        print('Введено не число, повторите ввод')

print(spisok)

# spisok = []
# x = ''
# while x != 'stop' and len(spisok) <5:
#     x = input('Введите число: ')
#     if x.isdecimal():
#         spisok.append(x)
#     elif x != 'stop':
#         print('Введено не число, повторите ввод')
#
# print(spisok)

