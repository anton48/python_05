#задание 1. создать список из 10 чисел. написать суммирование этого списка несколькими способами.

spisok = [23, 91, 49, 73, 24, 83, 67, 30, 48, 19]
print (spisok)

#вариант 1 - используем функцию sum
summa = sum(spisok)
print ('Сумма_1' + '=' + str(summa))

#вариант 2 - цикл
summa=0
for x in spisok:
    summa += x
print ('Сумма_2' + '=' + str(summa))

#вариант 3
summa=0
kolvo = len(spisok)
for i in range(kolvo):
   summa = summa + spisok[i]
print ('Сумма_3' + '=' + str(summa))

#вариант 4
summa=0
i=0
while i < len(spisok):
    summa =  summa + spisok[i]
    i = i + 1
print ('Сумма_4' + '=' + str(summa))

#5
import functools
summa = functools.reduce(lambda a, b: a + b, spisok, -7)
print ('Сумма_5' + '=' + str(summa))

